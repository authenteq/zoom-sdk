#import <UIKit/UIKit.h>

/** Represents the resolution options for the returned ZoOm Audit Trail Image(s) */
typedef NS_ENUM(NSInteger, ZoomAuditTrailType) {
    /** Configures ZoOm to disable returning Audit Trail Images. */
    ZoomAuditTrailTypeDisabled = 0,
    /** Configures ZoOm to return the fullest resolution image possible. */
    ZoomAuditTrailTypeFullResolution = 1,
    /** Configures Zoom to return images of height 640. */
    ZoomAuditTrailTypeHeight640 = 2,
};

/** Represents the options for the blur effect styles for the ZoOm Frame (area outside of ZoOm Oval). */
typedef NS_ENUM(NSInteger, ZoomBlurEffectStyle) {
    /** The blur effect will be off/disabled. */
    ZoomBlurEffectOff = 0,
    /** The blur effect will be default style (ONLY AVAILABLE ON IOS 10+) */
    ZoomBlurEffectStyleRegular = 1,
    /** The blur effect will have a light/white-tint style */
    ZoomBlurEffectStyleLight = 2,
    /** The blur effect will have a extra light/white-tint style */
    ZoomBlurEffectStyleExtraLight = 3,
    /** The blur effect will have a dark/black-tint style */
    ZoomBlurEffectStyleDark = 4,
    /** The blur effect will have a prominent style (ONLY AVAILABLE ON IOS 10+) */
    ZoomBlurEffectStyleProminent = 5,
};

/** Represents the options for placement of the ZoOm Cancel Button. */
typedef NS_ENUM(NSInteger, ZoomCancelButtonLocation) {
    /** ZoOm Cancel Button will appear in the top left. */
    ZoomCancelButtonLocationTopLeft = 0,
    /** ZoOm Cancel Button will appear in the top right. */
    ZoomCancelButtonLocationTopRight = 1,
    /** ZoOm Cancel Button will be disabled and hidden. */
    ZoomCancelButtonLocationDisabled = 2,
};

/** Represents the options for the behavior of iPhone X's view when frame size ratio is set to 1. */
typedef NS_ENUM(NSInteger, ZoomFullScreenBehavior) {
    /** ZoOm will handle the look of the view */
    ZoomFullScreenBehaviorAutomatic = 0,
    /** Developer is in full control of the look of ZoOm */
    ZoomFullScreenBehaviorManual = 1,
};

/** Represents the options for the transition animation used when dismissing the ZoOm Interface. */
typedef NS_ENUM(NSInteger, ZoomExitAnimationStyle) {
    /** Default. Quick fade out. */
    ZoomExitAnimationStyleNone = 0,
    /** Frame will fade out as oval and frame expand out quickly. */
    ZoomExitAnimationStyleRippleOut = 1,
    /** Frame will slowly fade out as oval and frame slowly expand out.*/
    ZoomExitAnimationStyleRippleOutSlow = 2,
};

@protocol ZoomSDKProtocol;

__attribute__((visibility("default")))
@interface Zoom: NSObject
@property (nonatomic, class, readonly, strong) id <ZoomSDKProtocol> _Nonnull sdk;
@end

@protocol ZoomFaceBiometricMetrics;
@protocol ZoomIDScanMetrics;
@class NSDate;

/** Represents the possible state of camera permissions. */
typedef NS_ENUM(NSInteger, ZoomCameraPermissionStatus) {
    /** The user has not yet been asked for permission to use the camera */
     ZoomCameraPermissionStatusNotDetermined = 0,
    /** The user denied the app permission to use the camera or manually revoked the app’s camera permission.
     From this state, permission can only be modified by the user from System ‘Settings’ context. */
    ZoomCameraPermissionStatusDenied = 1,
    /** The camera permission on this device has been disabled due to policy.
     From this state, permission can only be modified by the user from System ‘Settings’ context or contacting the system administrator. */
    ZoomCameraPermissionStatusRestricted = 2,
    /** The user granted permission to use the camera. */
    ZoomCameraPermissionStatusAuthorized = 3,
};

@class UIColor;
@class CAGradientLayer;
@class ZoomGuidanceCustomization;
@class ZoomGuidanceImagesCustomization;
@class ZoomOvalCustomization;
@class ZoomFeedbackCustomization;
@class ZoomCancelButtonCustomization;
@class ZoomFrameCustomization;
@class ZoomResultScreenCustomization;
@class ZoomOverlayCustomization;
@class ZoomIDScanCustomization;

/**
 * Class used to customize the look and feel of the ZoOm Interface.
 * ZoOm ships with a default ZoOm theme but has a variety of variables that you can use to configure ZoOm to your application's needs.
 * To customize the ZoOm Interface, simply create an instance of ZoomCustomization and set some, or all, of the variables.
 */
__attribute__((visibility("default")))
@interface ZoomCustomization : NSObject
/** Customize the ZoOm Identity Check Screens. */
@property (nonatomic, strong) ZoomIDScanCustomization * _Nonnull idScanCustomization;
/** Customize the ZoOm Overlay, separating the ZoOm Interface from the presenting application context. */
@property (nonatomic, strong) ZoomOverlayCustomization * _Nonnull overlayCustomization;
/** Customize the New User Guidance and Retry Screens. */
@property (nonatomic, strong) ZoomGuidanceCustomization * _Nonnull guidanceCustomization;
/** Customize the Result Screen. */
@property (nonatomic, strong) ZoomResultScreenCustomization * _Nonnull resultScreenCustomization;
/** Customize the ZoOm Oval and the ZoOm Progress Spinner animations. */
@property (nonatomic, strong) ZoomOvalCustomization * _Nonnull ovalCustomization;
/** Customize the ZoOm Feedback Bar. */
@property (nonatomic, strong) ZoomFeedbackCustomization * _Nonnull feedbackCustomization;
/** Customize the ZoOm Cancel Button. */
@property (nonatomic, strong) ZoomCancelButtonCustomization * _Nonnull cancelButtonCustomization;
/** Customize the ZoOm Frame. */
@property (nonatomic, strong) ZoomFrameCustomization * _Nonnull frameCustomization;

/**
 * Control the average amount of time we want to spend transitioning the user into the ZoOm Interface.
 * Default is 2.0.
 */
@property (nonatomic) float mainInterfaceEntryTransitionTime;

/**
 * Customize the transition out animation for an unsuccessful ZoOm Session.
 * Default is ZoomExitAnimationStyleNone.
 */
@property (nonatomic) enum ZoomExitAnimationStyle exitAnimationUnsuccess;
/**
 * Customize the transition out animation for a successful ZoOm Session.
 * Default is ZoomExitAnimationStyleNone.
 */
@property (nonatomic) enum ZoomExitAnimationStyle exitAnimationSuccess;

/**
 * This function allows special runtime control of the success message shown when the success animation occurs.
 * Please note that you can also customize this string via the standard customization/localization methods provided by ZoOm.
 * Special runtime access is enabled to this text because the developer may wish to change this text depending on ZoOm's mode of operation.
 * Default is "Success"
 */
+ (NSString * _Nullable) overrideResultScreenSuccessMessage;
+ (void) setOverrideResultScreenSuccessMessage:(NSString * _Nonnull)value;

@property (nonatomic) NSDictionary* _Nullable featureFlagsMap;

- (nonnull instancetype)init;
- (nonnull instancetype)initWithFeatureFlagsMap:(NSDictionary* _Nullable)featureFlagsMap  NS_SWIFT_NAME(init(featureFlagsMap:));
+ (nonnull instancetype)new;
@end


/** Represents results of a Zoom face biometric comparison */
@protocol ZoomFaceBiometricMetrics <NSObject>
/** Returns the Audit Trail Images as an array of base64 encoded JPG images. This should be considered the new default method of getting the Audit Trail Images.
 *  There are multiple advantages of using this new function. auditTrailCompressedBase64 provides a consistent API across all supported ZoOm platforms to
 *  get a compressed set of images that will not overly load user bandwidth and also provides images that are usable in the Audit Trail Verification API.
 */
@property (nonatomic, readonly, copy) NSArray<NSString *> * _Nullable auditTrailCompressedBase64;
/** Returns the Audit Trail Image. This functionality is being deprecated and will not exist in a future major release of ZoOm.
 *  Developers should instead use the auditTrailCompressedBase64 function. There are multiple advantages of using this new function.
 *  auditTrailCompressedBase64 provides a consistent API across all supported ZoOm platforms to get a compressed set of images that will not
 *  overly load user bandwidth and also provides images that are usable in the Audit Trail Verification API.
*/
@property (nonatomic, readonly, copy) NSArray<UIImage *> * _Nullable auditTrail;
/** A collection of the audit trails captured during the face analysis.  This parameter is nil unless Zoom.sdk.auditTrailType is set to something other than Disabled. */
@property (nonatomic, readonly, copy) NSArray<NSArray<UIImage *>*> * _Nullable auditTrailHistory;
/** A sample of time based images captured during the face analysis.  This parameter is nil unless enableTimeBasedSessionImages is set to true. */
@property (nonatomic, readonly, copy) NSArray * _Nullable timeBasedSessionImages;
/** ZoOm Biometric FaceMap. */
@property (nonatomic, readonly, copy) NSData * _Nullable faceMap;
/** ZoOm Biometric FaceMapBase64. */
@property (nonatomic, readonly, copy) NSString * _Nullable faceMapBase64;
/** ZoOm Biometric FaceMap. */
@property (nonatomic, readonly, copy) NSData * _Nullable zoomFacemap DEPRECATED_MSG_ATTRIBUTE("Use 'faceMap'");
@end // ZoomFaceBiometricMetrics

/** Represents results of Zoom ID Scan */
@protocol ZoomIDScanMetrics <NSObject>
/** A collection of the images captured during the ID Scan phase where users are instructed to present the front of their ID Document. */
@property (nonatomic, readonly, copy) NSArray<UIImage *> * _Nullable frontImages;
/** A collection of the images captured during the ID Scan phase where users are instructed to present the back of their ID Document.  When ID Type is passport, this object will return as any empty array. */
@property (nonatomic, readonly, copy) NSArray<UIImage *> * _Nullable backImages;
/** ZoOm ID Scan Data. */
@property (nonatomic, readonly, copy) NSData * _Nullable idScan;
/** ZoOm ID Scan Data as base64 encoded string. */
@property (nonatomic, readonly, copy) NSString * _Nullable idScanBase64;
/** A unique ID for the ZoOm ID Scan. */
@property (nonatomic, readonly, copy) NSString * _Nullable sessionId;
@end // ZoomIDScanMetrics

/**
 * Customize the ZoOm Identity Check Screens.
 */
__attribute__((visibility("default")))
@interface ZoomIDScanCustomization : NSObject
/**
 * Color of the Identity Document Type Selection Screen background.
 * Default is white.
 */
@property (nonatomic, copy) NSArray<UIColor *> * _Nonnull selectionScreenBackgroundColors;
/**
 * Applies a blur effect over the background of the Identity Document Type Selection Screen.
 * Default is ZoomBlurEffectOff.
 */
@property (nonatomic) enum ZoomBlurEffectStyle selectionScreenBlurEffectStyle;
/**
 * Conrol the opacity of the blur effect over the background of the Identity Document Type Selection Screen.
 * Values must be between 0 and 1.
 * Default is 1.
 */
@property (nonatomic) float selectionScreenBlurEffectOpacity;
/**
 * Color of the text displayed on the Identity Document Type Selection Screen (not including the action button text).
 * Default is dark grey.
 */
@property (nonatomic, strong) UIColor * _Nonnull selectionScreenForegroundColor;
/**
 * Color of the Identity Document Review Screen background.
 * Default is white.
 */
@property (nonatomic, copy) NSArray<UIColor *> * _Nonnull reviewScreenBackgroundColors;
/**
 * Applies a blur effect over the background of the Identity Document Review Screen.
 * Default is ZoomBlurEffectOff.
 */
@property (nonatomic) enum ZoomBlurEffectStyle reviewScreenBlurEffectStyle;
/**
 * Conrol the opacity of the blur effect over the background of the Identity Document Review Screen.
 * Values must be between 0 and 1.
 * Default is 1.
 */
@property (nonatomic) float reviewScreenBlurEffectOpacity;
/**
 * Color of the text displayed on the Identity Document Review Screen (not including the action button text).
 * Default is white.
 */
@property (nonatomic, strong) UIColor * _Nonnull reviewScreenForegroundColor;
/**
 * Color of the text view background during the Identity Document Review Screen.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull reviewScreenTextBackgroundColor;
/**
 * Color of the text view background border during the Identity Document Review Screen.
 * Default is transparent.
 */
@property (nonatomic, strong) UIColor * _Nonnull reviewScreenTextBackgroundBorderColor;
/**
 * Corner radius of the text view background and border during Identity Document Review Screen.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int reviewScreenTextBackgroundCornerRadius;
/**
 * Thickness of the text view background border during the Identity Document Review Screen.
 * Default is 0.
 */
@property (nonatomic) int reviewScreenTextBackgroundBorderWidth;
/**
 * Corner radius of the ID Document Preview image displayed on the Identity Document Review Screen.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int reviewScreenDocumentPreviewCornerRadius;
/**
 * Color of the text displayed on the Identity Document Capture Screen (not including the action button text).
 * Default is white.
 */
@property (nonatomic, strong) UIColor * _Nonnull captureScreenForegroundColor;
/**
 * Color of the text view background during the Identity Document Capture Screen.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull captureScreenTextBackgroundColor;
/**
 * Color of the text view background border during the Identity Document Capture Screen.
 * Default is transparent.
 */
@property (nonatomic, strong) UIColor * _Nonnull captureScreenTextBackgroundBorderColor;
/**
 * Corner radius of the text view background and border during Identity Document Capture Screen.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int captureScreenTextBackgroundCornerRadius;
/**
 * Thickness of the text view background border during the Identity Document Capture Screen.
 * Default is 0.
 */
@property (nonatomic) int captureScreenTextBackgroundBorderWidth;
/**
 * Color of the action button's text during Identity Check Screens.
 * Default is white.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonTextNormalColor;
/**
 * Color of the action button's background during Identity Check Screens.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonBackgroundNormalColor;
/**
 * Color of the action button's text when the button is pressed during Identity Check Screens.
 * Default is white.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonTextHighlightColor;
/**
 * Color of the action button's background when the button is pressed during Identity Check Screens.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonBackgroundHighlightColor;
/**
 * Color of the action button's border during Identity Check Screens.
 * Default is transparent.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonBorderColor;
/**
 * Thickness of the action button's border during Identity Check Screens.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int buttonBorderWidth;
/**
 * Corner radius of the action button's border during Identity Check Screens.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int buttonCornerRadius;
/**
 * Font of the title during the Identity Document Type Selection Screen.
 * Default is a semi-bold system font.
 */
@property (nonatomic, strong) UIFont * _Nonnull headerFont;
/**
 * Font of the message text during the Identity Document Capture and Review Screens.
 * Default is a light system font.
 */
@property (nonatomic, strong) UIFont * _Nonnull subtextFont;
/**
 * Font of the action button's text during the Identity Check Screens.
 * Default is a bold system font.
 */
@property (nonatomic, strong) UIFont * _Nonnull buttonFont;
/**
 * Controls whether to show the 'zoom_branding_logo_id_check' image (or image configured with .selectionScreenBrandingImage) on the Identity Document Type Selection Screen.
 * Default is true (visible).
 */
@property (nonatomic) BOOL showSelectionScreenBrandingImage;
/**
 * Image displayed on the Identity Document Type Selection Screen.
 * Default is configured to use image named 'zoom_branding_logo_id_check' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable selectionScreenBrandingImage;
/**
 * Image displayed on the Identity Document Capture Screen when the Identity Document Type selected is an ID Card.
 * Default is configured to use image named 'zoom_id_card_frame' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable captureScreenIDCardFrameImage;
/**
 * Image displayed on the Identity Document Capture Screen when the Identity Document Type selected is a Passport.
 * Default is configured to use image named 'zoom_passport_frame' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable captureScreenPassportFrameImage;
/**
 * Image displayed for the Torch button on the Identity Document Capture Screen when the torch/flashlight is active/on.
 * Default is configured to use image named 'zoom_active_torch' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable activeTorchButtonImage;
/**
 * Image displayed for the Torch button on the Identity Document Capture Screen when the torch/flashlight is inactive/off.
 * Default is configured to use image named 'zoom_inactive_torch' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable inactiveTorchButtonImage;


@end

/**
 * Customize the New User Guidance and Retry Screens.
 * New User Guidance Screens are shown before the ZoOm Session and Retry Screens are shown after an unsuccessful ZoOm Session.
 */
__attribute__((visibility("default")))
@interface ZoomGuidanceCustomization : NSObject
/**
 * Color of the background for the New User Guidance and Retry Screens.
 * Default is white.
 */
@property (nonatomic, copy) NSArray<UIColor *> * _Nonnull backgroundColors;
/**
 * Applies a blur effect over the background of the New User Guidance and Retry Screens.
 * Default is ZoomBlurEffectOff.
 */
@property (nonatomic) enum ZoomBlurEffectStyle blurEffectStyle;
/**
 * Conrol the opacity of the blur effect over the background of the New User Guidance and Retry Screens.
 * Values must be between 0 and 1.
 * Default is 1.
 */
@property (nonatomic) float blurEffectOpacity;
/**
 * Color of the text displayed on the New User Guidance and Retry Screens (not including the action button text).
 * Default is black.
 */
@property (nonatomic, strong) UIColor * _Nonnull foregroundColor;
/**
 * Color of the action button's text during the New User Guidance and Retry Screens.
 * Default is white.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonTextNormalColor;
/**
 * Color of the action button's background during the New User Guidance and Retry Screens.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonBackgroundNormalColor;
/**
 * Color of the action button's text when the button is pressed during the New User Guidance and Retry Screens.
 * Default is white.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonTextHighlightColor;
/**
 * Color of the action button's background when the button is pressed during the New User Guidance and Retry Screens.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonBackgroundHighlightColor;
/**
 * Color of the action button's border during the New User Guidance and Retry Screens.
 * Default is transparent.
 */
@property (nonatomic, strong) UIColor * _Nonnull buttonBorderColor;
/**
 * Thickness of the action button's border during the New User Guidance and Retry Screens.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int buttonBorderWidth;
/**
 * Corner radius of the action button's border during the New User Guidance and Retry Screens.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int buttonCornerRadius;
/**
 * Font of the title's subtext during the New User Guidance and Retry Screens.
 * Default is a semi-bold system font.
 */
@property (nonatomic, strong) UIFont * _Nonnull headerFont;
/**
 * Font of the title's subtext during the New User Guidance and Retry Screens.
 * Default is a light system font.
 */
@property (nonatomic, strong) UIFont * _Nonnull subtextFont;
/**
 * Font of the title's subtext during the New User Guidance and Retry Screens.
 * Default is a bold system font.
 */
@property (nonatomic, strong) UIFont * _Nonnull buttonFont;
/**
 * Background color of the Get Ready To ZoOm Screen text views during the New User Guidance and Retry Screens.
 * This will only be visible on iPhone 4/4s models.
 * Default is a semi-opaque shade of black.
 */
@property (nonatomic, strong) UIColor * _Nonnull readyScreenTextBackgroundColor;
/**
 * Background corner radius of the Get Ready To ZoOm Screen text views during the New User Guidance and Retry Screens.
 * This will only be visible on iPhone 4/4s models.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int readyScreenTextBackgroundCornerRadius;
/**
 * Color of the Get Ready To ZoOm Screen's oval fill.
 * Default is a semi-opaque custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull readyScreenOvalFillColor;
/**
 * Controls whether to show the 'zoom_branding_logo' image (or image configured with .imageCustomization.introScreenBrandingImage) on the first New User Guidance Screen.
 * Default is false (hidden).
 */
@property (nonatomic) BOOL showIntroScreenBrandingImage;
/** Customize the images used for the New User Guidance and Retry Screens. */
@property (nonatomic, strong) ZoomGuidanceImagesCustomization * _Nonnull imageCustomization;
- (nonnull instancetype) init;
@end

/**
 * Customize the images used for the New User Guidance and Retry Screens.
 */
__attribute__((visibility("default")))
@interface ZoomGuidanceImagesCustomization : NSObject

/**
 * Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.
 * Image displayed on the Lighting Instructions Slide (top image), shown on the second Retry Screen.
 * Default is configured to use image named 'zoom_good_lighting' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable goodLightingImage DEPRECATED_MSG_ATTRIBUTE("Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.");
/**
 * Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.
 * Image displayed on the Lighting Instructions Slide (bottom image). shown on the second Retry Screen.
 * Default is configured to use image named 'zoom_bad_side_lighting' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable badLightingImage DEPRECATED_MSG_ATTRIBUTE("Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.");
/**
 * Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.
 * Image displayed on the Face Angle Instructions Slide (top image), shown on the third Retry Screen.
 * Default is configured to use image named 'zoom_good_face_angle' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable goodAngleImage;
/**
 * Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.
 * Image displayed on the Face Angle Instructions Slide (bottom image), shown on the third Retry Screen.
 * Default is configured to use image named 'zoom_bad_face_angle' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable badAngleImage DEPRECATED_MSG_ATTRIBUTE("Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.");
/**
 * Image displayed as Ideal ZoOm example (right image) during the first Retry Screen.
 * Default is configured to use image named 'zoom_ideal' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable idealZoomImage;
/**
 * Image displayed on the Camera Permissions Screen.
 * Default is configured to use image named 'zoom_camera' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable cameraPermissionsScreenImage;
/**
 * Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.
 * Image displayed on the Lockout Screen while user is locked out of ZoOm.
 * Default is configured to use image named 'zoom_locked' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable lockoutScreenLockedImage DEPRECATED_MSG_ATTRIBUTE("Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.");
/**
 * Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.
 * Image displayed on the Lockout Screen when user's lockout time expires.
 * Default is configured to use image named 'zoom_unlocked' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable lockoutScreenUnlockedImage DEPRECATED_MSG_ATTRIBUTE("Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.");
/**
 * Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.
 * Image displayed on the Skip Guidance Button, shown during the Retry Screens.
 * Default is configured to use image named 'zoom_skip_guidance' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable skipGuidanceButtonImage DEPRECATED_MSG_ATTRIBUTE("Note: This functionality no longer exists in the ZoOm SDK. Using this API is now a no-op.");
/**
 * Image displayed on the first New User Guidance Screen.
 * Default is configured to use image named 'zoom_branding_logo' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable introScreenBrandingImage;
- (nonnull instancetype) init;
- (nonnull instancetype)initWithGoodLightingImage:(UIImage * _Nullable)goodLightingImage goodAngleImage:(UIImage * _Nullable)goodAngleImage badLightingImage:(UIImage * _Nullable)badLightingImage badAngleImage:(UIImage * _Nullable)badAngleImage idealZoomImage:(UIImage * _Nullable)idealZoomImage cameraPermissionsScreenImage:(UIImage * _Nullable)cameraPermissionsScreenImage lockoutScreenLockedImage:(UIImage * _Nullable)lockoutScreenLockedImage lockoutScreenUnlockedImage:(UIImage * _Nullable)lockoutScreenUnlockedImage errorScreenImage:(UIImage *_Nullable)errorScreenImage skipGuidanceButtonImage:(UIImage *_Nullable)skipGuidanceButtonImage introScreenBrandingImage:(UIImage * _Nullable)introScreenBrandingImage NS_SWIFT_NAME(init(goodLightingImage:goodAngleImage:badLightingImage:badAngleImage:idealZoomImage:cameraPermissionsScreenImage:lockoutScreenLockedImage:lockoutScreenUnlockedImage:errorScreenImage:skipGuidanceButtonImage:introScreenBrandingImage:));
@end

/**
 * Customize the Result Screen.
 * Shown for server-side work and response handling.
 */
__attribute__((visibility("default")))
@interface ZoomResultScreenCustomization : NSObject
/**
 * Color of the Result Screen's background.
 * Default is white.
 */
@property (nonatomic, copy) NSArray<UIColor *> * _Nonnull backgroundColors;
/**
 * Applies a blur effect over the background of the Result Screen.
 * Default is ZoomBlurEffectOff.
 */
@property (nonatomic) enum ZoomBlurEffectStyle blurEffectStyle;
/**
 * Conrol the opacity of the blur effect over the background of the Result Screen.
 * Values must be between 0 and 1.
 * Default is 1.
 */
@property (nonatomic) float blurEffectOpacity;
/**
 * Color of the text displayed on the Result Screen.
 * Default is black.
 */
@property (nonatomic, strong) UIColor * _Nonnull foregroundColor;
/**
 * Font of the message text displayed on the Result Screen.
 * Default is a system font.
 */
@property (nonatomic, strong) UIFont * _Nonnull messageFont;
/**
 * Color of the activity indicator animation shown during server-side work.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull activityIndicatorColor;
/**
 * Color of the result animation's background.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull resultAnimationBackgroundColor;
/**
 * Color of the result animation's accent color.
 * Default is white.
 */
@property (nonatomic, strong) UIColor * _Nonnull resultAnimationForegroundColor;
/**
 * Color of the upload progress bar's fill.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull uploadProgressFillColor;
/**
 * Color of upload progress bar's track.
 * Default is a semi-opaque shade of black.
 */
@property (nonatomic, strong) UIColor * _Nonnull uploadProgressTrackColor;
- (nonnull instancetype) init;
@end

/**
 * Customize the ZoOm Oval and the ZoOm Progress Spinner animations.
 */
__attribute__((visibility("default")))
@interface ZoomOvalCustomization : NSObject
/**
 * Color of the ZoOm Oval outline.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull strokeColor;
/**
 * Thickness of the ZoOm Oval outline.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int strokeWidth;
/**
 * Color of the animated ZoOm Progress Spinner strokes.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) UIColor * _Nonnull progressColor1;
@property (nonatomic, strong) UIColor * _Nonnull progressColor2;
/**
 * Radial offset of the animated ZoOm Progress Spinner strokes relative to the outermost bounds of the ZoOm Oval outline.
 * As this value increases, the ZoOm Progress Spinner stroke animations move closer toward the ZoOm Oval's center.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int progressRadialOffset;
/**
 * Thickness of the animated ZoOm Progress Spinner strokes.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int progressStrokeWidth;
- (nonnull instancetype) init;
@end

/**
 * Customize the ZoOm Feedback Bar.
 */
__attribute__((visibility("default")))
@interface ZoomFeedbackCustomization : NSObject
/**
 * Size of the ZoOm Feedback Bar, which is relative to the current .sizeRatio of the ZoOm Frame.
 * This customization is not available by default and exists for legacy support only.
 * Default is ZoOm picks the optimal Feedback Bar size depending on device size and camera support.
 */
@property (nonatomic) CGSize size;
/**
 * Vertical spacing of the Feedback Bar from the top boundary of the ZoOm Frame, which is relative to the current .sizeRatio of the ZoOm Frame.
 * This customization is not available by default and exists for legacy support only.
 * Default is ZoOm picks the optimal top margin depending on device size and camera support.
 */
@property (nonatomic) int topMargin;
/**
 * Corner radius of the ZoOm Feedback Bar.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int cornerRadius;
/**
 * Spacing between characters displayed within the ZoOm Feedback Bar.
 * Default is 1.5.
 */
@property (nonatomic) float textSpacing;
/**
 * Color of the text displayed within the ZoOm Feedback Bar.
 * Default is white.
 */
@property (nonatomic, strong) UIColor * _Nonnull textColor;
/**
 * Font of the text displayed within the ZoOm Feedback Bar.
 * Default is system font.
 */
@property (nonatomic) UIFont * _Nonnull textFont;
/**
 * Control whether to enable the pulsating-text animation within the ZoOm Feedback Bar.
 * Default is true (enabled).
 */
@property (nonatomic) BOOL enablePulsatingText;
/**
 * Color of the ZoOm Feedback Bar's background. Recommend making this have some transparency.
 * Default is custom ZoOm color.
 */
@property (nonatomic, strong) CAGradientLayer * _Nonnull backgroundColor;
- (nonnull instancetype) init;
@end

/**
 * Customize the ZoOm Frame.
 */
__attribute__((visibility("default")))
@interface ZoomFrameCustomization : NSObject
/**
 * Size ratio of the ZoOm Frame's width relative to the width the the current device's display.
 * This customization is not available by default and exists for legacy support only.
 * Default is ZoOm picks the optimal frame size depending on device size and camera support.
 */
@property (nonatomic) float sizeRatio;
/**
 * Vertical spacing of the ZoOm Frame from the top boundary of the current device's display.
 * This customization is not available by default and exists for legacy support only.
 * Default is ZoOm picks the optimal top margin depending on device size and camera support.
 */
@property (nonatomic) int topMargin;
/**
 * Corner radius of the ZoOm Frame.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int cornerRadius;
/**
 * Thickness of the ZoOm Frame's border.
 * Default is dynamically configured per device at runtime.
 */
@property (nonatomic) int borderWidth;
/**
 * Color of the ZoOm Frame's border.
 * Default is custom ZoOm color.
 */
@property (nonatomic) UIColor * _Nonnull borderColor;
/**
 * Color of the background surrounding the oval outline during ZoOm.
 * Default is custom ZoOm color.
 */
@property (nonatomic) UIColor * _Nonnull backgroundColor;
/**
 * Applies a blur effect over the background surrounding the oval outline during ZoOm.
 * Default is ZoomBlurEffectOff.
 */
@property (nonatomic) enum ZoomBlurEffectStyle blurEffectStyle;
/**
 * Conrol the opacity of the blur effect over the background surrounding the oval outline during ZoOm.
 * Values must be between 0 and 1.
 * Default is 1.
 */
@property (nonatomic) float blurEffectOpacity;
/**
 * Control behavior of the ZoOm Frame when .sizeRatio is set to 1.0.
 * Specific behavior for iPhone X models.
 * Default is ZoomFullScreenBehaviorAutomatic.
 */
@property (nonatomic) enum ZoomFullScreenBehavior fullScreenBehavior;
- (nonnull instancetype) init;
@end

/**
 * Customize the ZoOm Cancel Button.
 * Shown during ZoOm, New User Guidance, Retry, and Identity Check Screens.
 */
__attribute__((visibility("default")))
@interface ZoomCancelButtonCustomization : NSObject
/**
 * Image displayed on the ZoOm Cancel Button.
 * Default is configured to use image named 'zoom_cancel' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable customImage;
/**
 * Image displayed for the ZoOm Cancel Button when Low Light Mode is activated during ZoOm.
 * Default is configured to use image named 'zoom_cancel_low_light' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable customImageLowLight;
/**
 * Location, or use, of the ZoOm Cancel Button.
 * Default is ButtonLocationTopLeft.
 */
@property (nonatomic) enum ZoomCancelButtonLocation location;
- (nonnull instancetype) init;
@end

/**
 * Customize the ZoOm Overlay.
 * The ZoOm Overlay separates the ZoOm Interface from the presenting application, covering the device's full screen.
 */
__attribute__((visibility("default")))
@interface ZoomOverlayCustomization : NSObject
/**
 * Color of the ZoOm Overlay background.
 * Default is transparent.
 */
@property (nonatomic, copy) UIColor * _Nonnull backgroundColor;
/**
 * Applies a blur effect over the background of the ZoOm Overlay.
 * Default is ZoomBlurEffectOff.
 */
@property (nonatomic) enum ZoomBlurEffectStyle blurEffectStyle;
/**
 * Conrol the opacity of the blur effect over the background of the ZoOm Overlay.
 * Values must be between 0 and 1.
 * Default is 1.
 */
@property (nonatomic) float blurEffectOpacity;
/**
 * Image displayed below the ZoOm Frame on top of the ZoOm Overlay.
 * Default is configured to use image named 'zoom_your_app_logo' located in application's Assets folder.
 */
@property (nonatomic, strong) UIImage * _Nullable brandingImage;
- (nonnull instancetype) init;
@end

enum ZoomSDKStatus : NSInteger;
enum ZoomSessionStatus: NSInteger;
enum ZoomVerificationStatus: NSInteger;
enum ZoomIDScanStatus: NSInteger;
enum ZoomIDType: NSInteger;
enum ZoomIDScanRetryMode: NSInteger;

@class UIViewController;
@protocol ZoomSessionDelegate;
@protocol ZoomVerificationDelegate;
@protocol ZoomFaceMapProcessorDelegate;
@protocol ZoomFaceMapResultCallback;
@protocol ZoomIDScanProcessorDelegate;
@protocol ZoomIDScanResultCallback;

/**
 The ZoomSDKProtocol exposes methods the app can use to configure the behavior of ZoOm.
 */
@protocol ZoomSDKProtocol

/**
 Initialize the ZoOm SDK using your license key identifier for online validation.
 This <em>must</em> be called at least once by the application before invoking any SDK operations.
 This function may be called repeatedly without harm.

 @param licenseKeyIdentifier Identifies the client for determination of license capabilities
 @param preloadZoomSDK boolean to execute preload()
 @param completion Callback after license validation has completed
 */
- (void)initialize:(NSString * _Nonnull)licenseKeyIdentifier preloadZoomSDK:(BOOL)preloadZoomSDK completion:(void (^ _Nullable)(BOOL))completion NS_SWIFT_NAME(initialize(licenseKeyIdentifier:preloadZoomSDK:completion:));

/**
 Initialize the ZoOm SDK using your license key identifier for online validation.
 This <em>must</em> be called at least once by the application before invoking any SDK operations.
 This function may be called repeatedly without harm.
 
 @param licenseKeyIdentifier Identifies the client for determination of license capabilities
 @param completion Callback after license validation has completed
 */
- (void)initialize:(NSString * _Nonnull)licenseKeyIdentifier completion:(void (^ _Nullable)(BOOL))completion NS_SWIFT_NAME(initialize(licenseKeyIdentifier:completion:));

/**
 Initialize the ZoOm SDK using your license file for offline validation.
 This <em>must</em> be called at least once by the application before invoking any SDK operations.
 This function may be called repeatedly without harm.
 
 @param licenseText The string contents of the license file
 @param licenseKeyIdentifier Identifies the client
 @param preloadZoomSDK boolean to execute preload()
 @param completion Callback after license validation has completed
 */
- (void)initializeWithLicense:(NSString * _Nonnull)licenseText licenseKeyIdentifier:(NSString * _Nonnull)licenseKeyIdentifier preloadZoomSDK:(BOOL)preloadZoomSDK completion:(void (^ _Nullable)(BOOL))completion NS_SWIFT_NAME(initialize(licenseText:licenseKeyIdentifier:preloadZoomSDK:completion:));

/**
 Initialize the ZoOm SDK using your license file for offline validation.
 This <em>must</em> be called at least once by the application before invoking any SDK operations.
 This function may be called repeatedly without harm.
 
 @param licenseText The string contents of the license file
 @param licenseKeyIdentifier Identifies the client
 @param completion Callback after license validation has completed
 */
- (void)initializeWithLicense:(NSString * _Nonnull)licenseText licenseKeyIdentifier:(NSString * _Nonnull)licenseKeyIdentifier completion:(void (^ _Nullable)(BOOL))completion NS_SWIFT_NAME(initialize(licenseText:licenseKeyIdentifier:completion:));

/**
 Configures the look and feel of ZoOm.

 @param customization An instance of ZoomCustomization
 */
- (void)setCustomization:(ZoomCustomization * _Nonnull)customization;

/**
 Convenience method to check if the ZoOm Device License Key Identifier is valid.
 ZoOm requires that the app successfully initializes the SDK and receives confirmation of a valid Device License Key Identifier at least once before launching a ZoOm session.  After the initial validation, the SDK will allow a limited number of sessions without any further requirement for successful round-trip connection to the ZoOm server. This allows the app to use ZoOm for a limited time without network connectivity.  During this ‘grace period’, the function will return ‘true’.

 @return True, if the SDK license has been validated, false otherwise.
 */
- (BOOL)isLicenseValid;

/**
 Returns the current status of the ZoOm SDK.
 @return ZoomSDKStatusInitialized, if ready to be used.
 */
- (enum ZoomSDKStatus)getStatus;

/**
 Convenience method to get the time when a lockout will end.
 This will be null if the user is not locked out
 @return NSDate
 */
- (NSDate * _Nullable)getLockoutEndTime;

/**
 * @return True if the user is locked out of ZoOm
 */
- (BOOL)isLockedOut;

/**
 Preload ZoOm – this can be used to reduce the amount of time it takes to initialize a ZoOm view controller.  Preload is automatically called during initialize.
 You may want to call this function when transitioning to a ViewController in your application from which you intend to launch ZoOm.
 This ensures that ZoOm will launch as quickly as possible when requested.
 */
- (void)preload;

/**
 Unload resources related to ZoOm.
 */
- (void)unload;

/**
 Convenience method to check for camera permissions.
 This function is used to check the camera permission status prior to using ZoOm.  If camera permission has not been previously granted,
 ZoOm will display a UI asking the user to allow permission.  Some applications may wish to manage camera permission themselves - those applications
 should verify camera permissions prior to transitioning to ZoOm.

 @return Value representing the current camera permission status
 */
@property (nonatomic, readonly) enum ZoomCameraPermissionStatus cameraPermissionStatus;

/** Sets a prefered language to be used for all strings. */
- (void)setLanguage:(NSString * _Nonnull)language;

/**
 Configure where the ZoOm SDK looks for custom localized strings.
 @param table Optional name of the string table to look in.  By default, this is "Zoom" and string will be read from Zoom.strings.
 @param bundle Optional NSBundle instance to search for ZoOm string definitions in.  This will be searched after the main bundle and before ZoOm's default strings.
 */
- (void)configureLocalizationWithTable:(NSString * _Nullable)table bundle:(NSBundle * _Nullable)bundle;

/**
 Sets the type of audit trail images to be collected.
 If this property is not set to Disabled, then ZoOm will include a sample of some of the camera frames collected during the ZoOm session.
 */
@property (nonatomic) enum ZoomAuditTrailType auditTrailType;

/**
 Sets whether ZoOm will collect and return time based session images.
 If enabled this feature will return an array of optional UIImages whose size is dictated by the auditTrailType enum.
 */
@property (nonatomic) BOOL enableTimeBasedSessionImages;

/**
 Sets the time in seconds before a timeout occurs in the ZoOm session.
 This value has to be between 30 and 60 seconds. If it’s lower than 30 or higher than 60, it
 will be defaulted to 30 or 60 respectively.
 */
@property (nonatomic) NSInteger activeTimeoutInSeconds;

/**
 Fetches the version number of the current ZoOm SDK release
 
 @return Version number of sdk release package
 */
@property (nonatomic, readonly, copy) NSString * _Nonnull version;

/**
 Set the encryption key to be used for ZoOm Server FaceMaps
 
 @param publicKey RSA public key to be used in PEM format
 
 @return TRUE if the key was valid
 */
- (bool)setFaceMapEncryptionKeyWithPublicKey:(NSString * _Nonnull)publicKey NS_SWIFT_NAME(setFaceMapEncryptionKey(publicKey:));

/**
 * Method to create a valid string to pass as the value for the User-Agent header when calling the FaceTec Managed API.
 * @param sessionId Unique Id for a ZoOm session. This can be obtained from ZoomSessionResult.
 * @return a string that can be used as the value for the User-Agent header.
 */
- (NSString * _Nonnull)createZoomAPIUserAgentString:(NSString * _Nonnull)sessionId;

/**
 Configures and returns a new UIViewController for a ZoOm session.
 Caller should call presentViewController on returned object only once.
 
 @param delegate The delegate on which the application wishes to receive status results from the session.
 */
- (UIViewController * _Nonnull)createSessionVCWithDelegate:(id <ZoomSessionDelegate> _Nonnull)delegate NS_SWIFT_NAME(createSessionVC(delegate:));

- (UIViewController * _Nonnull)createSessionVCWithDelegate:(id <ZoomSessionDelegate> _Nonnull)delegate faceMapProcessorDelegate:(id <ZoomFaceMapProcessorDelegate> _Nullable)faceMapProcessorDelegate NS_SWIFT_NAME(createSessionVC(delegate:faceMapProcessorDelegate:));

- (UIViewController * _Nonnull)createSessionVCWithDelegate:(id <ZoomSessionDelegate> _Nonnull)delegate faceMapProcessorDelegate:(id <ZoomFaceMapProcessorDelegate> _Nullable)faceMapProcessorDelegate  zoomIDScanProcessorDelegate:(id <ZoomIDScanProcessorDelegate> _Nullable)zoomIDScanProcessorDelegate NS_SWIFT_NAME(createSessionVC(delegate:faceMapProcessorDelegate:zoomIDScanProcessorDelegate:));

/**
 Deprecated
 Configures and returns a new UIViewController that is used to launch a ZoOm Session.
 Caller should call presentViewController on returned object only once.
 
 @param delegate The delegate on which the application wishes to receive status results from the Verification
 */
- (UIViewController * _Nonnull)createVerificationVCWithDelegate:(id <ZoomVerificationDelegate> _Nonnull)delegate NS_SWIFT_NAME(createVerificationVC(delegate:)) DEPRECATED_MSG_ATTRIBUTE("Use createSessionVC...");

/** Returns a description string for a ZoomSessionStatus value */
- (NSString * _Nonnull)descriptionForSessionStatus:(enum ZoomSessionStatus)status;

/** Returns a description string for a ZoomSessionStatus value */
- (NSString * _Nonnull)descriptionForIDScanStatus:(enum ZoomIDScanStatus)status;

/** Returns a description string for a ZoomVerificationStatus value */
- (NSString * _Nonnull)descriptionForVerificationStatus:(enum ZoomVerificationStatus)status;

/** Returns a description string for a ZoomSDKStatus value */
- (NSString * _Nonnull)descriptionForSDKStatus:(enum ZoomSDKStatus)status;
@end

/** Represents the status of the SDK */
typedef NS_ENUM(NSInteger, ZoomSDKStatus) {
    /** Initialize was never attempted. */
    ZoomSDKStatusNeverInitialized = 0,
    /** The License provided was verified. */
    ZoomSDKStatusInitialized = 1,
    /** The Device License Key Identifier could not be verified due to connectivity issues on the user's device. */
    ZoomSDKStatusNetworkIssues = 2,
    /** The Device License Key Identifier provided was invalid. */
    ZoomSDKStatusInvalidDeviceLicenseKeyIdentifier = 3,
    /** This version of the ZoOm SDK is deprecated. */
    ZoomSDKStatusVersionDeprecated = 4,
    /** The Device License Key Identifier needs to be verified again. */
    ZoomSDKStatusOfflineSessionsExceeded = 5,
    /** An unknown error occurred. */
    ZoomSDKStatusUnknownError = 6,
    /** Device is locked out due to too many failures. */
    ZoomSDKStatusDeviceLockedOut = 7,
    /** Device is in landscape display orientation. ZoOm can only be used in portrait display orientation. */
    ZoomSDKStatusDeviceInLandscapeMode = 8,
    /** Device is in reverse portrait mode. ZoOm can only be used in portrait display orientation. */
    ZoomSDKStatusDeviceInReversePortraitMode = 9,
    /** License was expired, contained invalid text, or you are attempting to initialize in an App that is not specified in your license. */
    ZoomSDKStatusLicenseExpiredOrInvalid,
    /** The provided public encryption key is missing or invalid. */
    ZoomSDKStatusEncryptionKeyInvalid,
};

@protocol ZoomSessionResult;
@protocol ZoomVerificationResult;
@protocol ZoomIDScanResult;
enum ZoomSessionStatus : NSInteger;
enum ZoomVerificationStatus : NSInteger;

/**
 Applications should implement this delegate to receive results from a ZoomSession UIViewController.
 */
@protocol ZoomSessionDelegate <NSObject>
/**
 This method will be called exactly once after the ZoOm Session has completed and when NOT using the ZoomSession constructor with a ZoomFaceMapProcessor (i.e. Unmanaged Sessions).
 @param result A ZoomSessionResult instance.
 */
@optional
- (void)onZoomSessionCompleteWithResult:(id<ZoomSessionResult> _Nonnull)result NS_SWIFT_NAME(onZoomSessionComplete(result:));
/**
 This method will be called exactly once after the ZoOm Session has completed and when using the ZoomSession constructor with a ZoomFaceMapProcessor.
 */
@optional
- (void)onZoomSessionComplete NS_SWIFT_NAME(onZoomSessionComplete());
/**
 Optional callback function to be called when ZoOm is about to be dismissed.
 @param status The ZoomSessionStatus for the ZoOm Session.
 @return TRUE if you want to handle the dismissal of the ZoOm view controller.
 */
@optional
- (bool)onBeforeZoomDismissWithStatus:(enum ZoomSessionStatus)status NS_SWIFT_NAME(onBeforeZoomDismiss(status:)) DEPRECATED_MSG_ATTRIBUTE("Use onBeforeZoomDismissWithResult");
/**
 Optional callback function to be called when ZoOm is about to be dismissed.
 @param result The ZoomSessionResult for the ZoOm session.
 @return TRUE if you want to handle the dismissal of the ZoOm view controller.
 */
@optional
- (bool)onBeforeZoomDismissWithResult:(id<ZoomSessionResult> _Nonnull)result NS_SWIFT_NAME(onBeforeZoomDismiss(result:));
@end

/**
 ZoomFaceMapProcessorDelegate
 */
@protocol ZoomFaceMapProcessorDelegate <NSObject>
- (void)processZoomSessionResultWhileZoomWaits:(id<ZoomSessionResult> _Nonnull)zoomSessionResult zoomFaceMapResultCallback:(id<ZoomFaceMapResultCallback> _Nonnull)zoomFaceMapResultCallback NS_SWIFT_NAME(processZoomSessionResultWhileZoomWaits(zoomSessionResult:zoomFaceMapResultCallback:));
@end

/**
 ZoomIDScanProcessorDelegate
 */
@protocol ZoomIDScanProcessorDelegate <NSObject>
- (void)processZoomIDScanResultWhileZoomWaits:(id<ZoomIDScanResult> _Nonnull)zoomIDScanResult zoomIDScanResultCallback:(id<ZoomIDScanResultCallback> _Nonnull)zoomIDScanResultCallback NS_SWIFT_NAME(processZoomIDScanResultWhileZoomWaits(zoomIDScanResult:zoomIDScanResultCallback:));
@end


/**
 ZoomFaceMapResultCallback
 */
@protocol ZoomFaceMapResultCallback <NSObject>
- (void)onFaceMapUploadProgress:(float)uploadedPercent NS_SWIFT_NAME(onFaceMapUploadProgress(uploadedPercent:));
- (void)onFaceMapResultSucceed NS_SWIFT_NAME(onFaceMapResultSucceed());
- (void)onFaceMapResultRetry NS_SWIFT_NAME(onFaceMapResultRetry());
- (void)onFaceMapResultCancel NS_SWIFT_NAME(onFaceMapResultCancel());
@end

/**
 ZoomIDScanResultCallback
 */
@protocol ZoomIDScanResultCallback <NSObject>
- (void)onIDScanUploadProgress:(float)uploadedPercent NS_SWIFT_NAME(onIDScanUploadProgress(uploadedPercent:));
- (void)onIDScanResultSucceed NS_SWIFT_NAME(onIDScanResultSucceed());
- (void)onIDScanResultRetry:(enum ZoomIDScanRetryMode)retryMode NS_SWIFT_NAME(onIDScanResultRetry(retryMode:));
- (void)onIDScanResultCancel NS_SWIFT_NAME(onIDScanResultCancel());
@end

/**
 Deprecated.
 Applications should implement this delegate to receive results from a ZoomVerification UIViewController.
 */
DEPRECATED_MSG_ATTRIBUTE("Use ZoomSessionDelegate and createZoomSessionVC")
@protocol ZoomVerificationDelegate <NSObject>
/**
 This method will be called exactly once after the Zoom Session has completed.
 @param result A ZoomVerificationResult instance.
 */
- (void)onZoomVerificationResultWithResult:(id<ZoomVerificationResult> _Nonnull)result NS_SWIFT_NAME(onZoomVerificationResult(result:));
/**
 Optional callback function to be called when ZoOm is about to be dismissed.
 @param status The ZoomVerificationStatus for the ZoOm session.
 @return TRUE if you want to handle the dismissal of the ZoOm view controller.
 */
@optional
- (bool)onBeforeZoomDismissWithStatus:(enum ZoomVerificationStatus)status NS_SWIFT_NAME(onBeforeZoomDismiss(status:));
@end

/** Represents results of a Zoom Session Request */
@protocol ZoomSessionResult <NSObject>
/** Indicates whether the ZoOm Session was completed successfully or the cause of the unsuccess. */
@property (nonatomic, readonly) enum ZoomSessionStatus status;
/** Metrics collected during the ZoOm Session. */
@property (nonatomic, readonly, strong) id<ZoomFaceBiometricMetrics> _Nullable faceMetrics;
/** Number of full sessions (both retry and success) that the user performed from the time ZoOm was invoked to the time control is handed back to the application. */
@property (nonatomic, readonly) NSInteger countOfZoomSessionsPerformed;
/** Unique id for a ZoOm Session. */
@property (nonatomic, readonly, copy) NSString * _Nonnull sessionId;
@end

/** Represents results of a Zoom ID Scan */
@protocol ZoomIDScanResult <NSObject>
/** Indicates whether the ID Scan succeeded or the cause of failure. */
@property (nonatomic, readonly) enum ZoomIDScanStatus status;
/** Indicates the ID type. */
@property (nonatomic, readonly) enum ZoomIDType idType;
/** ID Scan Metrics */
@property (nonatomic, readonly, strong) id<ZoomIDScanMetrics> _Nullable idScanMetrics;
@end

/** Deprecated. Represents results of a Zoom Session Request */
DEPRECATED_ATTRIBUTE
@protocol ZoomVerificationResult <NSObject>
/** Indicates whether the ZoOm Session was completed successfully or the cause of the unsuccess. */
@property (nonatomic, readonly) enum ZoomVerificationStatus status;
/** Metrics collected during the ZoOm Session. */
@property (nonatomic, readonly, strong) id<ZoomFaceBiometricMetrics> _Nullable faceMetrics;
/** Number of ZoOm Sessions that the user performed from the time ZoOm was invoked to the time control is handed back to the application. */
@property (nonatomic, readonly) NSInteger countOfZoomSessionsPerformed;
/** Unique id for a ZoOm Session. */
@property (nonatomic, readonly, copy) NSString * _Nonnull sessionId;
@end

/** Represents the various end states of a ZoOm Session */
typedef NS_ENUM(NSInteger, ZoomSessionStatus) {
    /**
     The ZoOm Session was performed successfully and a FaceMap was generated.  Pass the FaceMap to ZoOm Server for further processing.
     */
    ZoomSessionStatusSessionCompletedSuccessfully,
    /**
     The ZoOm Session was not performed successfully and a FaceMap was not generated.  In general, other statuses will be sent to the developer for specific unsuccess reasons.
     */
    ZoomSessionStatusSessionUnsuccessful,
    /**
     The user pressed the cancel button and did not complete the ZoOm Session.
     */
    ZoomSessionStatusUserCancelled,
    /**
     This status will never be returned in a properly configured or production app.
     This status is returned if your license is invalid or network connectivity issues occur during a session when the application is not in production.
     */
    ZoomSessionStatusNonProductionModeLicenseInvalid,
    /**
     The camera access is prevented because either the user has explicitly denied permission or the user's device is configured to not allow access by a device policy.
     For more information on restricted by policy case, please see the the Apple Developer documentation on AVAuthorizationStatus.restricted.
     */
    ZoomSessionStatusCameraPermissionDenied,
    /**
     The ZoOm Session was cancelled due to the app being terminated, put to sleep, an OS notification, or the app was placed in the background.
     */
    ZoomSessionStatusContextSwitch,
    /**
     The ZoOm Session was cancelled because device is in landscape mode.
     The user experience of devices in these orientations is poor and thus portrait is required.
     */
    ZoomSessionStatusLandscapeModeNotAllowed,
    /**
     The ZoOm Session was cancelled because device is in reverse portrait mode.
     The user experience of devices in these orientations is poor and thus portrait is required.
     */
    ZoomSessionStatusReversePortraitNotAllowed,
    /**
     The ZoOm Session was cancelled because the user was unable to complete a ZoOm Session in the default allotted time or the timeout set by the developer.
     */
    ZoomSessionStatusTimeout,
    /**
     The ZoOm Session was cancelled due to memory pressure.
     */
    ZoomSessionStatusLowMemory,
    /**
     The ZoOm Session was cancelled because your App is not in production and requires a network connection.
     */
    ZoomSessionStatusNonProductionModeNetworkRequired,
    /**
     The ZoOm Session was cancelled because your License needs to be validated again.
     */
    ZoomSessionStatusGracePeriodExceeded,
    /**
     The ZoOm Session was cancelled because the developer-configured encryption key was not valid.
     */
    ZoomSessionStatusEncryptionKeyInvalid,
    /**
     The ZoOm Session was cancelled because not all guidance images were configured.
     */
     ZoomSessionStatusMissingGuidanceImages,
    /**
     The ZoOm Session was cancelled because ZoOm was unable to start the camera on this device.
     */
     ZoomSessionStatusCameraInitializationIssue,
    /**
     The ZoOm Session was cancelled because the user was in a locked out state.
     */
    ZoomSessionStatusLockedOut,
    /**
     The ZoOm Session was cancelled because of an unknown and unexpected error.  ZoOm leverages a variety of iOS APIs including camera, storage, security, networking, and more.
     This return value is a catch-all for errors experienced during normal usage of these APIs.
     */
    ZoomSessionStatusUnknownInternalError
};

/** Deprecated. Represents the various end states of a verification session */
DEPRECATED_ATTRIBUTE
typedef NS_ENUM(NSInteger, ZoomVerificationStatus) {
    /**
     The user was successfully processed. Device liveness and quality checks passed and a FaceMap was created.
     */
    ZoomVerificationStatusUserProcessedSuccessfully,
    /**
     The user was not processed successfully.
     This could be a liveness failure or a failure on the part of the user to perform a ZoOm correctly and/or with sufficiently good environmental conditions.
     A FaceMap was created if there were sufficient frames provided by the user.
     */
    ZoomVerificationStatusUserNotProcessed,
    /**
     The user cancelled out of the verification session rather than completing it.
     A FaceMap will not be created.
     */
    ZoomVerificationStatusFailedBecauseUserCancelled,
    /**
     When not using an offline license, ZoOm requires the developer to pass a valid Device License Key Identifier in order to function.
     This status will never be returned in a properly configured app, as the ZoOm APIs allow you to check Device License Key Identifier validity before invoking ZoOm UI.
     */
    ZoomVerificationStatusFailedBecauseAppTokenNotValid,
    /**
     The camera access is prevented because either the user has explicitly denied permission or
     the user's device is configured to not allow access by a device policy.
     For more information on restricted by policy case, please see the the Apple Developer documentation on AVAuthorizationStatus.restricted.
     */
    ZoomVerificationStatusFailedBecauseCameraPermissionDenied,
    /**
     Verification was terminated due to the app being terminated, put to sleep or to the background.
     A FaceMap will not be created.
     */
    ZoomVerificationStatusFailedBecauseOfOSContextSwitch,
    /**
     Verification was cancelled because device is in landscape mode.
     The user experience of devices in these orientations is poor and thus portrait is required.
     */
    ZoomVerificationStatusFailedBecauseOfLandscapeMode,
    /**
     Verification was cancelled because device is in reverse portrait mode.
     The user experience of devices in these orientations is poor and thus portrait is required.
     */
    ZoomVerificationStatusFailedBecauseOfReversePortraitMode,
    /**
     The user was unable to complete a session in the allotted time set by the developer.
     A FaceMap will not be created.
     */
    ZoomVerificationStatusFailedBecauseOfTimeout,
    /**
     Verification failed due to low memory.
     A FaceMap will not be created.
     */
    ZoomVerificationStatusFailedBecauseOfLowMemory,
    /**
     When not using an offline license, ZoOm requires network connection when being used.
     */
    ZoomVerificationStatusFailedBecauseNoConnectionInDevMode,
    /**
     When not using an offline license, ZoOm allows a number of sessions to occur without validating the Device License Key Identifier to handle scenarios where
     an end user might have lost connection to a network. Once that limit has been exceeded this failure will
     be returned.
     */
    ZoomVerificationStatusFailedBecauseOfflineSessionsExceeded,
    /**
     When configuring ZoOm to return FaceMaps, a valid ZoOm Server encryption key is required.
     Note: Liveness checks can occur without setting an encryption key.
     */
    ZoomVerificationStatusFailedBecauseEncryptionKeyInvalid,
    /**
     The ZoOm Session could not be started because not all guidance images were specified.  This indicates that ZoOm was not properly integrated/configured rather than an actual failure.
     */
    ZoomVerificationStatusFailedBecauseMissingGuidanceImages,
    /**
     The ZoOm Session was cancelled because the user was in a locked out state.
     */
    ZoomVerificationStatusLockedOut,
    /**
     Verification failed because of an unknown and unexpected error.
     ZoOm leverages a variety of iOS APIs including camera, storage, security, networking, and more.
     This return value is a catch-all for errors experienced during normal usage of these APIs.
     */
    ZoomVerificationStatusUnknownError
};

/** Represents the various end states of an ID Scan Session */
typedef NS_ENUM(NSInteger, ZoomIDScanStatus) {
    /**
     The ID Scan was performed successfully and identity document data was generated.
     */
    ZoomIDScanStatusSuccess,
    /**
     The ID Scan was not performed successfully and identity document data was not generated.
     In general, other statuses will be sent to the developer for specific unsuccess reasons.
     */
    ZoomIDScanStatusUnsuccess,
    /**
     The user pressed the cancel button and did not complete the ID Scan process.
     */
    ZoomIDScanStatusUserCancelled,
    /**
     The ID Scan was cancelled because the user was unable to complete an ID Scan in the default allotted time or the timeout set by the developer.
     */
    ZoomIDScanStatusTimedOut,
    /**
     The ID Scan was cancelled due to the app being terminated, put to sleep, an OS notification, or the app was placed in the background.
     */
    ZoomIDScanStatusContextSwitch,
    /**
     The ID Scan was cancelled due to an internal camera error.
     */
    ZoomIDScanStatusCameraError,
    /**
     The ID Scan was cancelled because a network connection is required.
     */
    ZoomIDScanStatusNetworkError,
    /**
     ID Scan cancelled because device is in landscape mode.
     The user experience of devices in these orientations is poor and thus portrait is required.
     */
    ZoomIDScanStatusLandscapeModeNotAllowed,
    /**
     ID Scan cancelled because device is in reverse portrait mode.
     The user experience of devices in these orientations is poor and thus portrait is required.
     */
    ZoomIDScanStatusReversePortraitNotAllowed
};

/** Represents the type of identity document for ID Scan */
typedef NS_ENUM(NSInteger, ZoomIDType) {
    /**
     ID card type
     */
    ZoomIDTypeIDCard,
    /**
     Passport type
     */
    ZoomIDTypePassport,
    /**
     ID type was not selected so it is unknown
     */
    ZoomIDTypeNotSelected
};

/** Represents the optionals available for retrying part or all of the ID Scan process */
typedef NS_ENUM(NSInteger, ZoomIDScanRetryMode) {
    ZoomIDScanRetryModeFront,
    ZoomIDScanRetryModeBack,
    ZoomIDScanRetryModeFrontAndBack
};
