Pod::Spec.new do |s|

  s.name         = "ZoomAuthenticationHybrid"
  s.version      = "7.0.18"
  s.summary      = "FaceTec's ZoOm iOS SDK - 3D Face Login + TrueLiveness"
  s.homepage     = "https://dev.zoomlogin.com"
  s.license      = { type: 'custom', text: 'ZoomAuthentication is Copyright 2018 FaceTec, Inc.  It may not be modified.' }
  s.author       = { "Gregory Perez" => "gperez@facetec.com" }

  s.platform     = :ios, "10.0"

    s.source      = {
        :git => 'https://AuthenteqSDK@bitbucket.org/authenteq/zoom-sdk.git',
        :tag => s.version
  }

  s.framework    = "ZoomAuthenticationHybrid"
  s.ios.deployment_target = '10.0.0'
  s.ios.vendored_frameworks = 'ZoomAuthenticationHybrid.framework'

end